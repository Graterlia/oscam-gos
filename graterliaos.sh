#!/bin/sh

clone()
{
	git clone git@gitlab.com:Graterlia/oscam-gos.git
}

commit()
{
	git add *
	#echo "Wpisz opis commita i nacisnij enter:"
	#read a
#	git commit -m "$a"
	git commit
	git push -u origin master
}

pull()
{
	#cd GraterliaOS
	git pull
}

git_empty_foldres_ignore()
{
	for nazwa in `find . -type d -empty`; do
		echo $nazwa
		touch "$nazwa/.graterila-git"
	done
}

git_empty_foldres_unignore()
{
	for nazwa in `find . -name '.graterila-git'`; do
		echo $nazwa
		rm "$nazwa"
	done
}

informacja()
{

	echo "Wybierz numer:"
	echo "1. clone → klonuje serwer GIT"
	echo "2. commit → wyślij na serwer GIT"
	echo "3. pull → uaktualnij dane na dysku z serwera GIT"
}

echo ""
informacja
read b
case "$b" in
	1)
		clone
		git_empty_foldres_unignore
	;;
	2)
		git_empty_foldres_ignore
		commit
		git_empty_foldres_unignore
	;;
	3)
		pull
	;;
	*)
		echo "Dokonałeś złego wyboru"
	;;
esac
